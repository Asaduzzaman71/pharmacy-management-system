 <label class="sidebar-label pd-x-10 mg-t-20 op-3">Navigation</label>
      <ul class="br-sideleft-menu">
        <li class="br-menu-item">
          <a href="index.html" class="br-menu-link active">
            <i class="menu-item-icon icon ion-ios-home-outline tx-24"></i>
            <span class="menu-item-label">Dashboard</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
      
      
        <li class="br-menu-item">
          <a href="#" class="br-menu-link with-sub">
            <i class="menu-item-icon fa fa-medkit tx-24"></i>
            <span class="menu-item-label">Manage Medicine</span>
          </a><!-- br-menu-link -->
          <ul class="br-menu-sub">
            <li class="sub-item"><a href="navigation.html" class="sub-link">Basic Nav</a></li>
            <li class="sub-item"><a href="navigation-layouts.html" class="sub-link">Nav Layouts</a></li>
            <li class="sub-item"><a href="navigation-effects.html" class="sub-link">Nav Effects</a></li>
          </ul>
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
          <a href="#" class="br-menu-link with-sub">
            <i class="menu-item-icon fa fa-search tx-20"></i>
            <span class="menu-item-label">Seacrh Medicine</span>
          </a><!-- br-menu-link -->
          <ul class="br-menu-sub">
            <li class="sub-item"><a href="chart-morris.html" class="sub-link">Morris Charts</a></li>
            <li class="sub-item"><a href="chart-flot.html" class="sub-link">Flot Charts</a></li>
            <li class="sub-item"><a href="chart-chartjs.html" class="sub-link">Chart JS</a></li>
            <li class="sub-item"><a href="chart-echarts.html" class="sub-link">ECharts</a></li>
            <li class="sub-item"><a href="chart-rickshaw.html" class="sub-link">Rickshaw</a></li>
            <li class="sub-item"><a href="chart-chartist.html" class="sub-link">Chartist</a></li>
            <li class="sub-item"><a href="chart-sparkline.html" class="sub-link">Sparkline</a></li>
            <li class="sub-item"><a href="chart-peity.html" class="sub-link">Peity</a></li>
          </ul>
        </li>
         <li class="br-menu-item">
          <a href="#" class="br-menu-link with-sub">
            <i class="menu-item-icon fa fa-medkit tx-24"></i>
            <span class="menu-item-label">Medicine Categories</span>
          </a><!-- br-menu-link -->
          <ul class="br-menu-sub">
            <li class="sub-item"><a href="{{route('Category.index')}}" class="sub-link">Category List</a></li>
            <li class="sub-item"><a href="{{route('Category.create')}}" class="sub-link">Add New Category</a></li>
            
          </ul>
        </li><!-- br-menu-item -->

       

      <br>