
 @extends('layouts.master')
 @section('css')


   
 @endsection
 @section('content')
 
     <br>
      

      <div class="br-pagebody">
        <div class="br-section-wrapper">

           <div class="row">

            <div class="col-lg-3">
                <h5>All Medicine Category List</h5>
            </div> 
            <div class="col-lg-3 offset-lg-6">        
                <a href="{{route('Category.create')}}" class="btn btn-primary">Add New Medicine Category</a>
            </div>
           </div>
          
         <br>

              <div class="bd bd-gray-300 rounded table-responsive">
            <table class="table table-striped mg-b-0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Category Name</th>
                  <th>Category Description</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($categories as $category)
                <tr>
                  <th scope="row">{{$category->id+1}}</th>
                  <td>{{$category->category_name}}</td>
                  <td>{!!$category->category_description!!}</td>
                  <td class="left">
                         @if($category->status==1)
                          <span class="label label-success">Active</span>
                          @else
                          <span class="label label-danger">Inactive</span>
                           @endif
                                    
                </td>
                <td>
                    <div class="row">
                      <div class="col-lg-2">
                        <a href="{{ route('Category.edit',$category->id)  }}" class="btn btn-sm btn-success" title="Edit">
                          <i class="fa fa-pencil-square-o"></i>
                          </a>
                        
                      </div>
                       <div class="col-lg-2">
                         <form action="{{route('Category.destroy', $category->id)}}" method="post">
                                @csrf
                                @method("DELETE")
                                
                             <button class="btn btn-sm btn-danger" onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash"></i></button>
                             </form> 
                       </div>
                       
                       </div>
                      
                              
                          


                   </td>
                </tr>
               @endforeach
               
              </tbody>
            </table>
          </div><!-- bd -->
        </div>
      @endsection

      @section('js')
  

  
      @endsection
